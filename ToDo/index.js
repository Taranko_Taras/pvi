var express = require('express');
var app = express();
var server = require('http').createServer(app);
var io = require('socket.io')(server);
const mongoose = require('mongoose');
const fs = require('fs');
const { error } = require('console');

const db = 'mongodb+srv://TFront:358145358145Qq@cluster0.xm4jfo0.mongodb.net/Messanger?retryWrites=true&w=majority';
const Login = require('./models/login');
const ToDo = require('./models/ToDo');
const { Namespace } = require('socket.io');

mongoose.connect(db, {useNewUrlParser: true, useUnifiedTopology: true})
.then((res) => console.log('Connected to db'))
.catch((error)=> console.log(error));


server.listen(3000);

app.get('/', function(request,response){
    
    response.sendFile(__dirname + '/struct.html');
    
});

users = [];
connections = [];

async function Register(login) {
    const existingLogin = await Login.findOne({ login });
    if (existingLogin) {
        console.log('User with such login already exist');
        return 0;
    }
    const loginn = new Login({login});
    loginn.save();
    console.log(login + 'registred');
    return 1;
}
async function adds(name) {
    const existingName = await ToDo.findOne({ name });
    if (existingName) {
        console.log('User with such login already exist');
        return 0;
    }
    return 1;
}
async function Logining(login) {
    const existingLogin = await Login.findOne({ login });
    if (existingLogin) {
        console.log('Logined');
        return 1;
    }
    console.log('No such user');
    return 0;
    
}

class ToDoClass {
    constructor(name,date,type,reciever,description) {
      this.name = name;
      this.date = date;
      this.type = type;
      this.reciever = reciever;
      this.description = description;
    }
};

io.sockets.on('connection', function(socket) {
    console.log("Connected");
    connections.push(socket);

    
    socket.on('disconnect', function(date){
        console.log("Disconnected");
        connections.splice(connections.indexOf(socket),1);
    });
    socket.on('addToDo',async function(data){

        var tempName = data.name;
        var temptime = data.time;
        var tempType = data.type;
        var tempReciever = data.recieverName;
        var tempDescription = data.description;
        const result = await adds(tempName);
        if(result === 1){
            const toDo = new ToDoClass(tempName, temptime, tempType, tempReciever,tempDescription);
            const todo = new ToDo(toDo);
            todo.save();
            io.sockets.emit('addToDoView', {name: toDo.name, time: toDo.date, type: toDo.type ,recieverName: tempReciever, description: tempDescription, err: 'Added'});
        }
        else{
            io.sockets.emit('addToDoView', {name: tempName, time: temptime, type: tempType ,recieverName: tempReciever, description: tempDescription, err: 'To do with such name already exist'});
        }
        
    });
    socket.on('changeToDo', async function(data){

        var tempName = data.name;
        var temptime = data.time;
        var tempType = data.type;
        var tempReciever = data.recieverName;
        var tempDescription = data.description;
        try {
            const updatedToDo = await ToDo.findOneAndUpdate(
              { name: tempName },
              { type: tempType,date:temptime, reciever: tempReciever, description: tempDescription },
              { new: true }
            );
        
            // Оновлення успішне
            console.log('Запис оновлено:', updatedToDo);
          } catch (err) {
            // Обробка помилки
            console.error(err);
          }
          io.sockets.emit('addToDoView', {name: tempName, time: temptime, type: tempType ,recieverName: tempReciever, description: tempDescription,err: 'Added'});
    });
    socket.on('register', async function(data){
        const result = await Register(data.login);
        if(result === 1){
            io.sockets.emit('registered', {mess: data.login , err: 'Registered'});
        }
        else{
            io.sockets.emit('registered', {mess: data.login, err: 'User with such login already exist'});
        }
        
    });
    socket.on('login', async function(data){
        const result = await Logining(data.login);
        if(result === 1){
            io.sockets.emit('logined', {mess: data.login , err: 'Logined'});
        }
        else{
            io.sockets.emit('logined', {mess: data.login, err: 'No such user, try to register'});
        }
        
    });
    socket.on('uploadToDo', async function(data){
        
        const toDo = await ToDo.find();
        toDo.forEach(function(td){
            console.log(td);
        });

        io.sockets.emit('updated', {td: toDo, updates: data.recieverName});      
    });
    socket.on('deleteToDo', async function(data){
        
        const query = { name: data.name };
        const result = await ToDo.deleteOne(query);  
        io.sockets.emit('deleted', {}); 
    });
});
