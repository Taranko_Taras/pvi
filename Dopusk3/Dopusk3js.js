const addMenu = document.querySelector(".modal");

const form = document.querySelector("#modal form");

form.addEventListener("submit", function(event) {
    event.preventDefault();
    const name = form.querySelector("#name").value;
    const surename = form.querySelector("#surename").value;
    var st = new Customer(name,surename);

    sendData(JSON.stringify(st));
    
  });

function sendData(data) {
  const xhr = new XMLHttpRequest();
  xhr.open("POST", "server.php", true);
  xhr.setRequestHeader("Content-Type", "application/json");
  xhr.onreadystatechange = function() {
    if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
      const response = JSON.parse(this.responseText);
      if (response.success) {
        alert("Congratulations");
        document.getElementById('modal').style.display= 'none';
      } else {
        for (const field in response.errors) {
          alert(response.errors[field]);      
        }  
      }
    }
  };
  
  xhr.send(data);
};
class Customer {
  constructor(name , surename) {
    this.name = name;
    this.surename = surename;
  }
}