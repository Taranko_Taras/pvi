<?php
header('Content-Type: application/json');

// зчитуємо дані з запиту
$data = json_decode(file_get_contents('php://input'), true);

// проводимо валідацію
$errors = array();
if ($data['name'] != "Taras") {
  $errors['name'] = 'Invalid name';
}
if ($data['surename'] != "Taranko") {
  $errors['surename'] = 'Invalid surename';
}

// відповідаємо на запит
if (count($errors) > 0) {
  $response = array('success' => false, 'errors' => $errors);
} else {
  $response = array('success' => true, 'message' => 'Дані успішно збережено');
}

echo json_encode($response);
?>