function validateFields() {
  var name = document.getElementById("nameInput").value;
  var surename = document.getElementById("surenameInput").value;
  var univercity = document.getElementById("myInput").value;
  var email = document.getElementById("emailInput").value;
  var phone = document.getElementById("phoneInput").value;
  var text = document.getElementById("textareaArea").value;
  var checkbox1 = document.getElementById("CCheckbox");
  var checkbox2 = document.getElementById("CCCheckbox");
  var checkbox3 = document.getElementById("CCCCheckbox");
  var checkbox4 = document.getElementById("JavaCheckbox");
  var checkbox5 = document.getElementById("JavaScriptCheckbox");
  
  var nameRegex = /^[a-zA-Zа-яА-ЯіІїЇёЁ]+$/;
  var isValidName = nameRegex.test(name);
  var surnameRegex = /^[a-zA-Zа-яА-ЯіІїЇёЁ]+$/;
  var isValidSurname = surnameRegex.test(surename);
  var emailRegex = /\S+@\S+\.\S+/;
  var isValidEmail = emailRegex.test(email);
  var phoneRegex =/^(\+38)?\s*\(?(0\d{2})\)?[-.\s]*(\d{3})[-.\s]*(\d{2})[-.\s]*(\d{2})$/;
  var isValidPhone = phoneRegex.test(phone);

  
  document.getElementById("nameInput").style.backgroundColor = "azure";
  document.getElementById("surenameInput").style.backgroundColor = "azure";
  document.getElementById("myInput").style.backgroundColor = "azure";
  document.getElementById("emailInput").style.backgroundColor = "azure";
  document.getElementById("phoneInput").style.backgroundColor = "azure";
  document.getElementById("textareaArea").style.backgroundColor = "azure";

  if (!isValidName) {
    document.getElementById("nameInput").style.backgroundColor = "pink";
    alert("The name must contain only letters");

    return false;
  }
  if (!isValidSurname) {
    alert("The last name must contain only letters");
    document.getElementById("surenameInput").style.backgroundColor = "pink";
    return false;
  }
  if (univercity.length == 0) {
    document.getElementById("myInput").style.backgroundColor = "pink";
    alert("Field univercity cant be empty");

    return false;
  }
  if (!isValidEmail) {
    alert("Please enter a valid email address");
    document.getElementById("emailInput").style.backgroundColor = "pink";
    return false;
  }
  if (!isValidPhone) {
    alert("Please enter a valid phone number");
    document.getElementById("phoneInput").style.backgroundColor = "pink";
    return false;
  }
  if (!checkbox1.checked && !checkbox2.checked && !checkbox3.checked && !checkbox4.checked && !checkbox5.checked) {
    nonChoose();
    return false;
  }
  if (text.length == 0) {
    document.getElementById("textareaArea").style.backgroundColor = "pink";
    alert("Field quastions cant be empty");

    return false;
  }
  document.getElementById("maindiv").style.display = "none";
  
  alert("Congratulations");
}

function nonChoose() {
  document.getElementById("maindiv").style.display = "none";
  document.getElementById("RegisterForm").style.display = "block";
};
function okButtn(){
  document.getElementById("maindiv").style.display = "none";
  document.getElementById("RegisterForm").style.display = "none";
  alert("Congratulations");
};
function closeForm(){
  document.getElementById("maindiv").style.display = "block";
  document.getElementById("RegisterForm").style.display = "none";
};
function cancelLast(){
  document.getElementById("maindiv").style.display = "none";
};
function openDiv(){
  document.getElementById("RegButtn").style.display = "none";
  document.getElementById("maindiv").style.display = "block";
}
  