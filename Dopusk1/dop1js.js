function showKlick() {
  document.getElementById('inp').style.display = 'block';
  document.getElementById('show').style.display = 'none';
};

function addKlick() {
  var ul = document.getElementById("myUL");
  var li = document.createElement("li");
  var checkbox = document.createElement('input');
        checkbox.type = "checkbox";
        checkbox.value = 1;
        checkbox.name = "todo[]";
  li.appendChild(checkbox);
  
  var a = document.createElement('a');
  a.href = 'https://www.selfverified.store/';
  a.textContent = document.getElementById('inp').value;
  li.appendChild(checkbox);
  li.appendChild(a);
  ul.appendChild(li);
  ul.appendChild(li)
};

function deleteSelected() {
  var ul = document.getElementById("myUL");
  var items = ul.getElementsByTagName("li");
  for (var i = 0; i < items.length; ++i) {
    if (items[i].getElementsByTagName("input")[0].checked) {
      ul.removeChild(items[i]);
      --i;
    }
  }
};
