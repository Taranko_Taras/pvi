function validateFields() {
  var city = document.getElementById("myInput").value;
  var startDate = document.getElementById("dateStart").value;
  var endtDate = document.getElementById("dateEnd").value;
  var name = document.getElementById("nameInput").value;
  var surname = document.getElementById("surenameInput").value;
  var age = document.getElementById("ageInput").value;
  var email = document.getElementById("emailInput").value;
  var checkbox = document.getElementById("rightsCheckbox");
  
  var nameRegex = /^[a-zA-Zа-яА-ЯіІїЇёЁ]+$/;
  var isValidName = nameRegex.test(name);
  var surnameRegex = /^[a-zA-Zа-яА-ЯіІїЇёЁ]+$/;
  var isValidSurname = surnameRegex.test(surname);
  var isValidAge = age >= 18 && age <= 100;
  var emailRegex = /\S+@\S+\.\S+/;
  var isValidEmail = emailRegex.test(email);
  
  document.getElementById("myInput").style.backgroundColor = "white";
  document.getElementById("myList").style.backgroundColor = "white";
  document.getElementById("nameInput").style.backgroundColor = "white";
  document.getElementById("surenameInput").style.backgroundColor = "white";
  document.getElementById("ageInput").style.backgroundColor = "white";
  document.getElementById("emailInput").style.backgroundColor = "white";
  document.getElementById("dateStart").style.backgroundColor = "white";
  document.getElementById("dateEnd").style.backgroundColor = "white";


  if (city.length == 0) {
    document.getElementById("myInput").style.backgroundColor = "pink";
    alert("Field city cant be empty");

    return false;
  }

  if(startDate == "" ||endtDate == "" || startDate > endtDate){
    alert("Invalid date");
    document.getElementById("dateStart").style.backgroundColor = "pink";
    document.getElementById("dateEnd").style.backgroundColor = "pink";
    return false;
  }
  if (!isValidName) {
    document.getElementById("nameInput").style.backgroundColor = "pink";
    alert("The name must contain only letters");

    return false;
  }

  
  if (!isValidSurname) {
    alert("The last name must contain only letters");
    document.getElementById("surenameInput").style.backgroundColor = "pink";
    return false;
  }
  
  if (!isValidAge) {
    alert("Age must be a number between 18 and 100");
    document.getElementById("ageInput").style.backgroundColor = "pink";
    return false;
  }
  if (checkbox.checked) {
  } else {
    alert("You must have driver license");
    return false;
  }
  if (!isValidEmail) {
    alert("Please enter a valid email address");
    document.getElementById("emailInput").style.backgroundColor = "pink";
    return false;
  }
  
  alert("Congratulations");

}