let btns = document.querySelectorAll('button');
let i = 0;

let car = document.querySelector('.car');

let timer = setInterval(() => {
    if( (i%3) == 0)
    {
        btns[0].style.backgroundColor = 'red';
        btns[1].style.backgroundColor = 'gray';
        btns[2].style.backgroundColor = 'gray';

        car.classList.remove('car-anim');
    }

    if( (i%3) == 1)
    {
        btns[0].style.backgroundColor = 'gray';
        btns[1].style.backgroundColor = 'yellow';
        btns[2].style.backgroundColor = 'gray';

        car.classList.add('car-anim');
    }

    if( (i%3) == 2)
    {
        btns[0].style.backgroundColor = 'gray';
        btns[1].style.backgroundColor = 'gray';
        btns[2].style.backgroundColor = 'green';
    }
    if( (i%3) == 3)
    {
        btns[0].style.backgroundColor = 'red';
        btns[1].style.backgroundColor = 'gray';
        btns[2].style.backgroundColor = 'gray';

        car.classList.remove('car-anim');
    }

    if( (i%3) == 4)
    {
        btns[0].style.backgroundColor = 'gray';
        btns[1].style.backgroundColor = 'yellow';
        btns[2].style.backgroundColor = 'gray';

        car.classList.add('car-anim');
    }

    if( (i%3) == 5)
    {
        btns[0].style.backgroundColor = 'gray';
        btns[1].style.backgroundColor = 'gray';
        btns[2].style.backgroundColor = 'green';
    }

    i++;
}, 2000);