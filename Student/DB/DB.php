<?php


    class DB
    {
        protected $pdo;
        protected static $instance;
        private function __construct()
        {
            $settings = require __DIR__.'/settings.php';

            $this->pdo = new PDO(
                "mysql:host={$settings['host']};dbname={$settings['dbname']}",
                $settings['username'],
                $settings['pass']
            );
        }

        public static function getInstance(): self
        {
            if(self::$instance === NULL){
                self::$instance = new self;
            }
            return self::$instance;
        }

        public function select($table)
        {
            $sql = "SELECT * FROM {$table}";
            return $this->query($sql);
        }

        public function selectById($table, $id)
        {
            $sql = "SELECT * FROM {$table}
            WHERE stud_id = $id";

            return $this->query($sql);
        }


        public function insertInto($table, $param)
        {
            foreach ($param as $column => $value){
                $columns[] = $column;
                $valuesIndex[] = ':'.$column;
                $paramToValues[':'.$column] = $value;
            }

            $sql = 'INSERT INTO `'.$table.'` (' . implode(', ', $columns).') VALUE ('.implode(', ', $valuesIndex).');';

            //var_dump($sql);

            return $this->query($sql, $paramToValues);
        }

        public function query(string $sql, array $params = [])
        {
            $sth = $this->pdo->prepare($sql);
            $result = $sth->execute($params);

            if ($result === false){
                return null;
            }
            
        
            return $sth->fetchAll();
        }

        public function getLastId()
        {
            return $this->pdo->lastInsertId();
        }

        public function updateElement($table, $param, $id)
        {
            if(Empty($this->selectById($table, $id)))
            {
                //var_dump($this->selectById($table, $id));
                return array(1,2,3);
            }
            foreach ($param as $column => $value)
            {
                $columns[] = $column;
                $valuesIndex[] = $column;
                $paramToValues[] = $value;
                
            }
            //dd($paramToValues);
            $sql = "UPDATE `$table` SET 
            stud_name = '$paramToValues[0]', stud_group = $paramToValues[1], stud_gender = '$paramToValues[2]', stud_birthday = '$paramToValues[3]', stud_status = $paramToValues[4]
            WHERE stud_id = $id";
            //var_dump($sql);
            
            return $this->query($sql);
        }

        public function deleteElem($table ,$id)
        {
            $sql = "DELETE FROM $table
            WHERE stud_id = $id";
            return $this->query($sql);
        }
        public function getLoggined($table, $login, $password)
        {
            $sql = "SELECT * FROM {$table}
            WHERE stud_login = '$login' AND stud_password = '$password'";

            return $this->query($sql);
        }


    }

    function dd(...$arg)
    {
        echo '<pre>';
        var_dump($arg);
        echo '</pre>';
        exit();
    }
?>