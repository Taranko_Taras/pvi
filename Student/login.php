<?php
    header('Content-Type: application/json');
    $data = json_decode(file_get_contents('php://input'), true);

    $login = $data['login'];
    $password = $data['password'];

    require __DIR__."/DB/DB.php";
    try
    {
        $db = DB::getInstance();
    }
    catch(PDOException $e)
    {
        $response = array('success' => false, 'errors' => "1");
        echo json_encode($response);
        exit();
    }

    if(Empty($db->getLoggined("logins", $login, $password)))
    {
        
        $response = array('success' => false, 'errors' => "3");
        echo json_encode($response);
        exit();
    }

    $response = array('success' => true, 'message' => 'Loggined');
    echo json_encode($response);
?>