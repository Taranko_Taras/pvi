let rowCount = 1;
let tempRow = 0;
let addOrEdit = 0;
let isLoggined = 0;

var tableBody = document.getElementById("tableBody");
var createStud = document.getElementById("createStud");
var closeAdd = document.getElementById("closeAdd");

var deleteBtn = document.getElementById("deleteBtn");
var canselBtn = document.getElementById("canselBtn");

var login = document.getElementById("login");
var closeLogin = document.getElementById("closeLogin");
var getLogined = document.getElementById("getLogined");

var add = document.getElementById("add");
class students 
{
    constructor(id ,name, group, gender, birthday, status) 
    {
        this.id = id;
        this.name = name;
        this.group = group;
        this.gender = gender;
        this.birthday = birthday;
        this.status = 0;
    }
}
class yourLogin
{
    constructor(login, password) 
    {
        this.login = login;
        this.password = password;
    }
}
createStud.onclick = function()
{
    if(addOrEdit == 0)
    {
        createStudent();
    }
    else
    {
        editStudent();
    }
}

login.onclick = function()
{
    if(!isLoggined)
    {
        var popup = document.getElementById("loginDiv");
        popup.classList.toggle("show");
    }
    if(isLoggined){
        location.reload();
    }
}

closeLogin.onclick = function()
{
    var popup = document.getElementById("loginDiv");
    popup.classList.toggle("show");
}

getLogined.onclick = function()
{
    if(!isLoggined)
    {
        var setLogin = document.getElementById("setLogin");
        var setPassword = document.getElementById("setPassword");

        var data = new yourLogin(setLogin.value, setPassword.value);

        sendLogin(JSON.stringify(data));
    }
}

function sendLogin(data)
{
    const xhr = new XMLHttpRequest();
    xhr.open("POST", "login.php", true);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.onreadystatechange = function() 
    {
        if (this.readyState === XMLHttpRequest.DONE && this.status === 200) 
        {
            const response = JSON.parse(this.responseText);
            if (response.success)
            {
                alert("You are loggined!");
                isLoggined = 1;
                var popup = document.getElementById("loginDiv");
                popup.classList.toggle("show");

                var setLogin = document.getElementById("setLogin");
                var setPassword = document.getElementById("setPassword");
                setLogin.value = "";
                setPassword.value = "";
                login.textContent = "Log Out"
                onLogined();
                add.style.visibility = "visible";
            }
            else 
            {
                for (const field in response.errors) 
                {
                    alert(response.errors[field]);      
                }  
            }
        }
    };
    xhr.send(data);
}

function onLogined() 
{
    const xhr = new XMLHttpRequest();
    xhr.open("POST", "serverOnOpen.php", true);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.onreadystatechange = function() 
    {
        if (this.readyState === XMLHttpRequest.DONE && this.status === 200) 
        {
            const response = JSON.parse(this.responseText);
            if (response.success)
            {
                alert("Student updated!");
                console.log(response.objects);
                addFromBD(response.objects);
            } 
            else 
            {
                for (const field in response.errors) 
                {
                    alert(response.errors[field]);      
                }  
            }
        }
    };
    xhr.send();
}


function sendData(data) 
{
    const xhr = new XMLHttpRequest();
    if(addOrEdit == 0)
    {
        xhr.open("POST", "server.php", true);
        xhr.setRequestHeader("Content-Type", "application/json");
        xhr.onreadystatechange = function() 
        {
        if (this.readyState === XMLHttpRequest.DONE && this.status === 200) 
        {
            const response = JSON.parse(this.responseText);
            if (response.success) 
            {
                alert("Student done!");
                data.id = response.index;
                console.log(data);
                changeValueAdd(response.index);
            } 
            else 
            {
                for (const field in response.errors) 
                {
                    alert(response.errors[field]);      
                }  
            }
            var popup = document.getElementById("addStud");
            popup.classList.toggle("show");

            var setName = document.getElementById("setName");
            var setGroup = document.getElementById("setGroup");
            var setGenderM = document.getElementById("setGenderM");
            var setGenderW = document.getElementById("setGenderW");
            var setBirthday = document.getElementById("setBirthday");

            setName.value = "";
            setGroup.selectedIndex = 0;
            setGenderM.checked = false;
            setGenderW.checked = false;
            setBirthday.value = "";
        }
        };
    }
    else
    {
        xhr.open("POST", "change.php", true);
        xhr.setRequestHeader("Content-Type", "application/json");
        xhr.onreadystatechange = function() 
        {
        if (this.readyState === XMLHttpRequest.DONE && this.status === 200) 
        {
            console.log(this.responseText);
            const response = JSON.parse(this.responseText);
            if (response.success) 
            {
                alert("Student done!");
                console.log(data);
                changeValue();
            } 
            else
            {
                for (const field in response.errors) 
                {
                    alert(response.errors[field]);      
                }    
            }

            var popup = document.getElementById("addStud");
            popup.classList.toggle("show");

            var setName = document.getElementById("setName");
            var setGroup = document.getElementById("setGroup");
            var setGenderM = document.getElementById("setGenderM");
            var setGenderW = document.getElementById("setGenderW");
            var setBirthday = document.getElementById("setBirthday");

            setName.value = "";
            setGroup.selectedIndex = 0;
            setGenderM.checked = false;
            setGenderW.checked = false;
            setBirthday.value = "";
        }
        };
    }

    
   
    xhr.send(data);
};

function addFromBD(data)
{
    data.forEach(element => 
    {
        var startTabB = document.createElement("tr");
        startTabB.rowIndex = element.stud_id;

        //checkbox
        var tempTd = document.createElement("td");
        var inputT = document.createElement("input");
        inputT.type = "checkbox";
        inputT.id = "checkbox"+element.stud_id;
        tempTd.appendChild(inputT);
        startTabB.appendChild(tempTd);

        //group
        tempTd = document.createElement("td");
        if(element.stud_group == 1)
        {
            tempTd.textContent = "PZ-22";
        }
        else if(element.stud_group == 2)
        {
            tempTd.textContent = "PZ-23";
        }
        tempTd.id = "group"+element.stud_id;
        startTabB.appendChild(tempTd);

        //name
        tempTd = document.createElement("td");
        tempTd.textContent = element.stud_name;
        tempTd.id = "name"+element.stud_id;
        startTabB.appendChild(tempTd);

        //gender
        tempTd = document.createElement("td");
        if(element.stud_gender == "W")
        {
            tempTd.textContent = "Woman";
        }
        else if(element.stud_gender == "M")
        {
            tempTd.textContent = "Man";   
        }
        tempTd.id = "gender"+element.stud_id;
        startTabB.appendChild(tempTd);

        //birthday
        tempTd = document.createElement("td");
        tempTd.textContent = element.stud_birthday;
        tempTd.id = "birthday"+element.stud_id;
        startTabB.appendChild(tempTd);
        
        //status
        tempTd = document.createElement("td");
        var divTemp = document.createElement("div");
        divTemp.classList.add("round");
        tempTd.id = "status"+element.stud_id;
        tempTd.appendChild(divTemp);
        startTabB.appendChild(tempTd);

        //options

        //Edit
        tempTd = document.createElement("td");
        var buttonTemp = document.createElement("button");
        buttonTemp.textContent = "Edit";
        buttonTemp.id = "editStudBtn";
        buttonTemp.onclick = function()
        {
            var popup = document.getElementById("addStud");
            popup.classList.toggle("show");
            tempRow = element.stud_id;
            addOrEdit = 1;
        }
        tempTd.appendChild(buttonTemp);


        //Remove
        buttonTemp = document.createElement("button");
        buttonTemp.textContent = "Remove";
        buttonTemp.onclick = function()
        {
            var popup = document.getElementById("deleteStud");
            popup.classList.toggle("show");
            deleteBtn.onclick = function()
            {
                removeBD(element.stud_id);
                tableBody.removeChild(startTabB);
                var popup = document.getElementById("deleteStud");
                popup.classList.toggle("show");

            }
        }
        tempTd.appendChild(buttonTemp);

        startTabB.appendChild(tempTd);
        tableBody.appendChild(startTabB);
    });
}

function editStudent()
{
    var setName = document.getElementById("setName");
    var setGroup = document.getElementById("setGroup");
    var setGenderM = document.getElementById("setGenderM");
    var setGenderW = document.getElementById("setGenderW");
    var setBirthday = document.getElementById("setBirthday");


    var temp = new students(tempRow, setName.value, setGroup.selectedIndex, "G", setBirthday.value);  
    if(setGenderM.checked)
    {
        temp.gender = "M";
    }
    else if(setGenderW.checked)
    {
        temp.gender = "W";
    }
    console.log(JSON.stringify(temp));
    sendData(JSON.stringify(temp));
}

function changeValue()
{
    var cellName = document.getElementById("name"+tempRow);
    var cellGroup = document.getElementById("group"+tempRow);
    var cellGender = document.getElementById("gender"+tempRow);
    var cellBirthday = document.getElementById("birthday"+tempRow);

    var setName = document.getElementById("setName");
    var setGroup = document.getElementById("setGroup");
    var setGenderM = document.getElementById("setGenderM");
    var setGenderW = document.getElementById("setGenderW");
    var setBirthday = document.getElementById("setBirthday");

    cellName.textContent = setName.value;
    if(setGroup.selectedIndex == 1)
    {
        cellGroup.textContent = "PZ-22";
    }
    else if(setGroup.selectedIndex == 2)
    {
        cellGroup.textContent = "PZ-23";
    }
    cellBirthday.textContent = setBirthday.value;
    if(setGenderW.checked)
    {
        cellGender.textContent = "Woman";
    }
    else
    {
        cellGender.textContent = "Man";   
    }
}

canselBtn.onclick = function()
{
    var popup = document.getElementById("deleteStud");
        popup.classList.toggle("show");
}

closeAdd.onclick = function()
{
    var popup = document.getElementById("addStud");
    popup.classList.toggle("show");
}

function changeValueAdd(temp)
{
    var setName = document.getElementById("setName");
    var setGroup = document.getElementById("setGroup");
    var setGenderM = document.getElementById("setGenderM");
    var setGenderW = document.getElementById("setGenderW");
    var setBirthday = document.getElementById("setBirthday");
    var elementId = document.getElementById("elementId");

    var startTabB = document.createElement("tr");
    startTabB.rowIndex = temp;

    //checkbox
    var tempTd = document.createElement("td");
    var inputT = document.createElement("input");
    inputT.type = "checkbox";
    inputT.id = "checkbox"+temp;
    tempTd.appendChild(inputT);
    startTabB.appendChild(tempTd);

    //group
    tempTd = document.createElement("td");
    if(setGroup.selectedIndex == 1)
    {
        tempTd.textContent = "PZ-22";
    }
    else if(setGroup.selectedIndex == 2)
    {
        tempTd.textContent = "PZ-23";
    }
    tempTd.id = "group"+temp;
    startTabB.appendChild(tempTd);

    //name
    tempTd = document.createElement("td");
    tempTd.textContent = setName.value;
    tempTd.id = "name"+temp;
    startTabB.appendChild(tempTd);

    //gender
    tempTd = document.createElement("td");
    if(setGenderM.checked && setGenderW.checked)
    {
        alert("Wrong gender!");
        return false;
    }
    else if(setGenderW.checked)
    {
        tempTd.textContent = "Woman";
    }
    else if(setGenderM.checked)
    {
        tempTd.textContent = "Man";   
    }
    else
    {
        alert("Wrong gender!");
        return false;
    }
    tempTd.id = "gender"+temp;
    startTabB.appendChild(tempTd);

    //birthday
    tempTd = document.createElement("td");
    tempTd.textContent = setBirthday.value;
    tempTd.id = "birthday"+temp;
    startTabB.appendChild(tempTd);
    
    //status
    tempTd = document.createElement("td");
    var divTemp = document.createElement("div");
    divTemp.classList.add("round");
    tempTd.id = "status"+temp;
    tempTd.appendChild(divTemp);
    startTabB.appendChild(tempTd);

    //options

    //Edit
    tempTd = document.createElement("td");
    var buttonTemp = document.createElement("button");
    buttonTemp.textContent = "Edit";
    buttonTemp.id = "editStudBtn";
    buttonTemp.onclick = function()
    {
        var popup = document.getElementById("addStud");
        popup.classList.toggle("show");
        tempRow = temp;
        addOrEdit = 1;
    }
    tempTd.appendChild(buttonTemp);


    //Remove
    buttonTemp = document.createElement("button");
    buttonTemp.textContent = "Remove";
    buttonTemp.onclick = function()
    {
        var popup = document.getElementById("deleteStud");
        popup.classList.toggle("show");
        deleteBtn.onclick = function()
        {
            removeBD(temp);
            tableBody.removeChild(startTabB);
            var popup = document.getElementById("deleteStud");
            popup.classList.toggle("show");

        }
    }
    //id
    elementId.value = temp;

    tempTd.appendChild(buttonTemp);

    startTabB.appendChild(tempTd);
    tableBody.appendChild(startTabB);
}

function removeBD(index)
{
    const xhr = new XMLHttpRequest();
    xhr.open("POST", "remove.php", true);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.onreadystatechange = function() 
    {
    if (this.readyState === XMLHttpRequest.DONE && this.status === 200) 
    {
        const response = JSON.parse(this.responseText);
        if (response.success) 
        {
            return;
        } 
        else 
        {
            for (const field in response.errors) 
            {
                alert(response.errors[field]);      
            }  
        }
    }
    };

    xhr.send(index);
}

function createStudent()
{
    var setName = document.getElementById("setName");
    var setGroup = document.getElementById("setGroup");
    var setGenderM = document.getElementById("setGenderM");
    var setGenderW = document.getElementById("setGenderW");
    var setBirthday = document.getElementById("setBirthday");

    var temp = new students(rowCount, setName.value, setGroup.selectedIndex, "G", setBirthday.value);  
    if(setGenderM.checked)
    {
        temp.gender = "M";
    }
    else if(setGenderW.checked)
    {
        temp.gender = "W";
    }
    console.log(JSON.stringify(temp));

    sendData(JSON.stringify(temp));
}

function profilePop() 
{
    var popup = document.getElementById("profilePop");
    popup.classList.toggle("show");
}

function notificPop()
{
    var popup = document.getElementById("notificPop");
    popup.classList.toggle("show");
}

function addStud()
{
    var popup = document.getElementById("addStud");
    popup.classList.toggle("show");
    addOrEdit = 0;
}
