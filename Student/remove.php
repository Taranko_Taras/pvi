<?php
    header('Content-Type: application/json');
    $data = file_get_contents('php://input');
    
    require __DIR__."/DB/DB.php";
    try
    {
        $db = DB::getInstance();
    }
    catch(PDOException $e){
        $response = array('success' => false, 'errors' => "1");
        echo json_encode($response);
        exit();
    }

    
    $db->deleteElem("student", $data);

    $response = array('success' => true, 'message' => 'Deleted');
    echo json_encode($response);
?>