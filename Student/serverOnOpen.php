<?php
    header('Content-Type: application/json');

    require __DIR__."/DB/DB.php";
    try
    {
        $db = DB::getInstance();
    }
    catch(PDOException $e){
        $response = array('success' => false, 'errors' => "1");
        echo json_encode($response);
        exit();
    }
    $students = $db->select("student");

    /*
    $sql = "SELECT * FROM `student`";
    $stmt = $conn->prepare($sql);
    $result = $stmt->execute();
    $students = $stmt->fetchAll();
    //dd($students);
    */
    $response = array('success' => true, 'message' => 'Added', 'objects' => $students);
    echo json_encode($response);
?>