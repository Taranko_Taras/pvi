<?php
    header('Content-Type: application/json');
    $data = json_decode(file_get_contents('php://input'), true);
    $errors = array();

    if (!preg_match('/^[A-ZА-ЯІЇЄҐ][a-zа-яіїєґ]+$/', $data['name'])) 
    {
        $errors['name'] = 'Wrong name';
    }
    elseif (strtotime($data['birthday']) > strtotime(date('Y-m-d'))) 
    {
        $errors['birthday'] = 'Invalid birth date';
    }
    elseif($data['group'] <= 0)
    {
        $errors['group'] = 'Invalid group';
    }
    elseif($data['gender'] == 'G')
    {
        $errors['gender'] = 'Invalid gender';
    }

    if (count($errors) > 0) 
    {
        $response = array('success' => false, 'errors' => $errors);
        echo json_encode($response);
        exit();
    }

    $name = $data['name'];
    $group = $data['group'];
    $gender = $data['gender'];
    $birthday = $data['birthday'];

    require __DIR__."/DB/DB.php";
    try
    {
        $db = DB::getInstance();
    }
    catch(PDOException $e){
        $response = array('success' => false, 'errors' => "1");
        echo json_encode($response);
        exit();
    }
    $db->insertInto("student", 
    [
        "stud_name" => $name,
        "stud_group" => $group,
        "stud_gender" => $gender,
        "stud_birthday" => $birthday,
        "stud_status" => "0",
    ]);

    $last_id = $db->getLastId();

    $response = array('success' => true, 'message' => 'Added', 'index' => $last_id);
    echo json_encode($response);
?>