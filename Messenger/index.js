var express = require('express');
var app = express();
var server = require('http').createServer(app);
var io = require('socket.io')(server);
const mongoose = require('mongoose');
const fs = require('fs');
const { error } = require('console');

const db = 'mongodb+srv://Nickname:PASSWORD@cluster0.xm4jfo0.mongodb.net/Messanger?retryWrites=true&w=majority';
const Login = require('./models/login');
const Messange = require('./models/messange');

mongoose.connect(db, {useNewUrlParser: true, useUnifiedTopology: true})
.then((res) => console.log('Connected to db'))
.catch((error)=> console.log(error));


server.listen(3000);

app.get('/', function(request,response){
    
    response.sendFile(__dirname + '/struct.html');
    
});

users = [];
connections = [];

async function Register(login) {
    const existingLogin = await Login.findOne({ login });
    if (existingLogin) {
        console.log('User with such login already exist');
        return 0;
    }
    const loginn = new Login({login});
    loginn.save();
    console.log(login + 'registred');
    return 1;
}

async function Logining(login) {
    const existingLogin = await Login.findOne({ login });
    if (existingLogin) {
        console.log('Logined');
        return 1;
    }
    console.log('No such user');
    return 0;
    
}

class MessageClass {
    constructor(mess,name,recieverName,className) {
      this.mess = mess;
      this.name = name;
      this.recieverName = recieverName;
      this.className = className;
    }
};

io.sockets.on('connection', function(socket) {
    console.log("Connected");
    connections.push(socket);

    
    socket.on('disconnect', function(date){
        console.log("Disconnected");
        connections.splice(connections.indexOf(socket),1);
    });
    socket.on('send mess', function(data){
        console.log(data.name);
        var tempMess = data.mess;
        var tempName = data.name;
        var tempReciever = data.recieverName;
        var tempClass = data.className;

        const messange = new Messange({
            mess: tempMess,
            name: tempName,
            recieverName: tempReciever,
            className: tempClass
        });
        messange.save();
        io.sockets.emit('add mess', {mess: data.mess, name: data.name, recieverName: data.recieverName, className: data.className});
    });
    socket.on('register', async function(data){
        const result = await Register(data.login);
        if(result === 1){
            io.sockets.emit('registered', {mess: data.login , err: 'Registered'});
        }
        else{
            io.sockets.emit('registered', {mess: data.login, err: 'User with such login already exist'});
        }
        
    });
    socket.on('login', async function(data){
        const result = await Logining(data.login);
        if(result === 1){
            io.sockets.emit('logined', {mess: data.login , err: 'Logined'});
        }
        else{
            io.sockets.emit('logined', {mess: data.login, err: 'No such user, try to register'});
        }
        
    });
    socket.on('uploadMessages', async function(data){
        
        const messes = await Messange.find();
        //const mess = messes.map(doc => doc.mess);
        //const name = messes.map(doc => doc.name);
        //const recieverName = messes.map(doc => doc.recieverName);
        //const className = messes.map(doc => doc.className);
        messes.forEach(function(mass){
            console.log(mass);
        });

        io.sockets.emit('updated', {mess: messes, updates: data.recieverName});      
    });
});
