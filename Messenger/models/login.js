const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const loginSchema = new Schema({
    login:{
        type: String,
        required: true,
    }
});

const Login = mongoose.model('Login',loginSchema);

module.exports = Login;
