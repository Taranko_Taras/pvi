var items = document.getElementsByClassName("item");
var circle = document.getElementById("circle");
var circleWidth = circle.offsetWidth;
var circleHeight = circle.offsetHeight;

function moveCircle(currentItem, targetItem) {
  circle.style.left = currentItem.offsetLeft - circleWidth + "px";
  circle.style.top = currentItem.offsetTop + "px";
  
  setTimeout(function() {
    circle.style.left = targetItem.offsetLeft - circleWidth + "px";
    circle.style.top = targetItem.offsetTop + "px";
    circle.style.opacity = "1";
  }, 500);
}

for (var i = 0; i < items.length; i++) {
  items[i].addEventListener("click", function(e) {
    e.preventDefault();
    var currentItem = this;
    var targetItem = null;
    if (currentItem == items[0]) {
      targetItem = items[1];
      moveCircle(currentItem, targetItem);
      setTimeout(function() {
        targetItem = items[2];
        moveCircle(items[1], targetItem);
        setTimeout(function() {
          circle.style.opacity = "0";
        }, 1000);
      }, 1000);
    } else if (currentItem == items[1]) {
      targetItem = items[2];
      moveCircle(currentItem, targetItem);
      setTimeout(function() {
        circle.style.opacity = "0";
      }, 1000);
    } else {
      targetItem = items[1];
      moveCircle(currentItem, targetItem);
      setTimeout(function() {
        targetItem = items[0];
        moveCircle(items[1], targetItem);
        setTimeout(function() {
          circle.style.opacity = "0";
        }, 1000);
      }, 1000);
    }
  });
}